//My first JSON API using express
const express = require('express');
const server = express();

//Middleware
server.use('/Info', () => {
    console.log('Info MiddleWare is Active...');
    
});

server.use(express.json());

server.get('/', (req, res) => {
	res.json({message: 'OK'});
});

server.get('/home', (req, res) => {
	res.json({message: 'Home'});
});

server.get('/info', (req, res) => {
	res.json({message: 'Info'});
});

server.listen(3333, () => console.log('System is listening on port 127.0.0.1:3333'))

